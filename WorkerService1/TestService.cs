﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.Text;

namespace WorkerService1
{
    public class TestService
    {
        private BackgroundJobServer _server;
        public TestService()
        {
            _server = new BackgroundJobServer();

        }



        public bool Testing()
        {
            BackgroundJob.Enqueue(() => Console.WriteLine("Hello, world!"));
            using (var server = new BackgroundJobServer())
            {
                //var res = Console.ReadLine();
                Console.WriteLine("starting background service");
            }
            //check here if recurring job is running - if not start it
            RecurringJob.AddOrUpdate("CallingStuff_Method", () => CallingStuff(), Cron.Minutely);


            return true;
        }


        public void CallingStuff()
        {
            Console.WriteLine("Woohoo");
        }

    }
}
